#  create_ap wlan0 eth0 MyAccessPoint MyPassPhrase
#  echo -e 'MyAccessPoint\nMyPassPhrase' | create_ap wlan0 eth0
#  create_ap wlan0 eth0 MyAccessPoint
#  echo 'MyAccessPoint' | create_ap wlan0 eth0
#  create_ap wlan0 wlan0 MyAccessPoint MyPassPhrase
#  create_ap -n wlan0 MyAccessPoint MyPassPhrase
#  create_ap -m bridge wlan0 eth0 MyAccessPoint MyPassPhrase
#  create_ap -m bridge wlan0 br0 MyAccessPoint MyPassPhrase
#  create_ap --driver rtl871xdrv wlan0 eth0 MyAccessPoint MyPassPhrase
#  create_ap --daemon wlan0 eth0 MyAccessPoint MyPassPhrase
#  create_ap --stop wlan0

#!/usr/bin/env bash
hostpost_dir="$(cd "$(dirname "$0")" && pwd)"
hostpost_key=$1

#conf=$(cat ${hostpost_dir}/confs/hostpost.conf.json |  jq '.[] | select(.hostpost_active)')
conf=$(cat ${hostpost_dir}/confs/hostpost.conf.json |  jq ".${hostpost_key}")

hostpost_name=$(echo ${conf} | jq -r '.hostpost_name')
hostpost_senha=$(echo ${conf} | jq -r '.hostpost_senha')
hostpost_source=$(echo ${conf} | jq -r '.hostpost_source')
hostpost_share=$(echo ${conf} | jq -r '.hostpost_share')
hostpost_active=$(echo ${conf} | jq -r '.hostpost_active')

echo ${hostpost_share}
create_ap --stop ${hostpost_share}




