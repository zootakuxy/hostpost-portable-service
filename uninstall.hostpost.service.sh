#!/usr/bin/env bash

#save file template.service.conf on /etc/systemd/system/template.service

service_base_dir=/etc/systemd/system

read -p "Name of service: " service_name


service_location=${service_base_dir}/${service_name}.service


echo Service ${service_name} uninstaled on ${service_location}

systemctl stop ${service_name}
systemctl disable ${service_name}
rm ${service_location}
#rm ${service_location} symlinks that might be related
systemctl daemon-reload
systemctl reset-failed




