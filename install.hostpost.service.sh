#!/usr/bin/env bash

#save file template.service.conf on /etc/systemd/system/template.service

service_base_dir=/etc/systemd/system
hostpost_dir="$(cd "$(dirname "$0")" && pwd)"

read -p "Name of service: " service_name
read -p "Host post key: " hostpost_key


hostpost_dir="$(cd "$(dirname "$0")" && pwd)"
#pwdesc=$(echo $PWD | sed 's_/_\\/_g')

service_data=$(cat ${hostpost_dir}/confs/template.service.conf)

service_name=${service_name}.service
service_location=${service_base_dir}/${service_name}

replace_location_new=$(cd "$(dirname "$0")" && pwd | sed 's_/_\\/_g')
replace_location_old="\$(hostpost_dir)"
service_data=$(echo "${service_data}" | sed -e "s/${replace_location_old}/${replace_location_new}/g" )

replace_key_old="\$(hostpost_key)"
replace_key_new=${hostpost_key}
echo "${service_data}" | sed -e "s/${replace_key_old}/${replace_key_new}/g" > ${service_location}

systemctl daemon-reload
systemctl reset-failed

echo Service ${service_name} instaled on ${service_location}




